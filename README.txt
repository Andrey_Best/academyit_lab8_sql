Business DataBase Entity Editor v. 1.0.0

Very comfortable database editor for supervisor-representative checking business.

Native UI with understandable buttons.

To start application unzip "EntityFrameworkPractice.zip" and open "EntityFrameworkPractice.exe"


To add new row in database - use "Adds" buttons, and fulfill required info.

To get tables from database - use "Shows" buttons.
Requested data will be shown in the bottom of window in Data Base Grid.


Made with EntityFramework 6.4 and in WPF with C#.

Made with love by Andrey Kotliar <3