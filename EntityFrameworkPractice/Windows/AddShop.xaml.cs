﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;

namespace EntityFrameworkPractice.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddShop.xaml
    /// </summary>
    public partial class AddShop : Window
    {
        private Kotliar_AndreyEntities DB;

        public AddShop(Kotliar_AndreyEntities DB)
        {
            this.DB = DB;
            InitializeComponent();
        }

        private void WindowAddShop_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                List<User> representatives = new List<User>();
                var query = from R in DB.representatives
                            select new
                            {
                                R.users.first_name,
                                R.users.last_name,
                                R.representative_id
                            };

                foreach (var representative in query.ToList())
                {
                    representatives.Add(new User(representative.first_name, representative.last_name, representative.representative_id));
                }

                for (int i = 0; i < representatives.Count; i++)
                {
                    comboBoxRepresentatives.Items.Add(representatives[i]);
                }

                if (representatives.Count > 0)
                {
                    comboBoxRepresentatives.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonAddShop_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxAddress.Text != "" && textBoxName.Text != "" && comboBoxRepresentatives.Text != "")
            {
                try
                {
                    shops shop = new shops
                    {
                        address = textBoxAddress.Text,
                        name = textBoxName.Text,
                        representative_id = (comboBoxRepresentatives.SelectedItem as User).id
                    };
                    DB.shops.Add(shop);
                    DB.SaveChanges();

                    MessageBox.Show(Constants.successfullQueryMessage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(Constants.fieldfillingWarning);
            }
        }
    }
}
