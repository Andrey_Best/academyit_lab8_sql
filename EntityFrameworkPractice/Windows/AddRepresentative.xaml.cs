﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;

namespace EntityFrameworkPractice.Windows
{
    /// <summary>
    /// Логика взаимодействия для AddRepresentative.xaml
    /// </summary>
    public partial class AddRepresentative : Window
    {
        private Kotliar_AndreyEntities DB;

        public AddRepresentative(Kotliar_AndreyEntities DB)
        {
            this.DB = DB;
            InitializeComponent();
        }

        private void WindowAddRepresentative_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                List<User> supervisors = new List<User>();
                var query = from R in DB.supervisors select new
                {
                    R.supervisor_id,
                    R.users
                };

                foreach (var supervisor in query.ToList())
                {
                    supervisors.Add(new User(supervisor.users.first_name, supervisor.users.last_name, supervisor.supervisor_id));
                }

                for (int i = 0; i < supervisors.Count; i++)
                {
                    comboBoxSuperVisors.Items.Add(supervisors[i]);
                }

                if (supervisors.Count > 0)
                {
                    comboBoxSuperVisors.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonAddRepresentative_Click(object sender, RoutedEventArgs e)
        {
            if (textBoxFirstName.Text != "" && textBoxLastName.Text != "" && textBoxCity.Text != "" && textBoxPhoneNumber.Text != "" && comboBoxSuperVisors.Text != "")
            {
                try
                {
                    users user = new users
                    {
                        first_name = textBoxFirstName.Text,
                        last_name = textBoxLastName.Text,
                        city = textBoxCity.Text,
                        phone_number = textBoxPhoneNumber.Text
                    };
                    DB.users.Add(user);
                    DB.SaveChanges();

                    representatives representative = new representatives
                    {
                        user_id = user.user_id,
                        supervisor_id = (comboBoxSuperVisors.SelectedValue as User).id
                    };
                    DB.representatives.Add(representative);
                    DB.SaveChanges();

                    MessageBox.Show(Constants.successfullQueryMessage);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(Constants.fieldfillingWarning);
            }
        }

    }
}
