﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameworkPractice
{
    class Constants
    {
        public const string fieldfillingWarning = "All fields should be fulfilled!";
        public const string successfullQueryMessage = "Action completed successfully!";
    }
}
