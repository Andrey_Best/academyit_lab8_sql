﻿using EntityFrameworkPractice.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace EntityFrameworkPractice
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Kotliar_AndreyEntities DB = new Kotliar_AndreyEntities();

        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            FillSupervisorsComboBox();
            FillRepresentativesComboBox();
            FillTopicsComboBox();
            FillGridWithRepresentatives();
        }

        private void ButtonShowRepresentatives_Click(object sender, RoutedEventArgs e)
        {
            FillGridWithRepresentatives();
        }

        private void FillGridWithRepresentatives()
        {
            var query = from R in DB.representatives
                        select new
                        {
                            R.users.first_name,
                            R.users.last_name,
                            R.users.city,
                            R.users.phone_number,
                            supervisors_first_name = R.supervisors.users.first_name,
                            supervisors_last_name = R.supervisors.users.last_name
                        };
            dataGridTables.ItemsSource = query.ToList();
        }

        private void ButtonShowShops_Click(object sender, RoutedEventArgs e)
        {
            FillGridWithShops();
        }

        private void FillGridWithShops()
        {
            var query = from R in DB.shops
                        select new
                        {
                            R.address,
                            R.name,
                            representative_first_name = R.representatives.users.first_name,
                            representative_last_name = R.representatives.users.last_name,
                        };
            dataGridTables.ItemsSource = query.ToList();
        }

        private void FillRepresentativesComboBox()
        {
            try
            {
                List<User> representatives = new List<User>();
                var query = from R in DB.representatives
                            select new
                            {
                                R.users.first_name,
                                R.users.last_name,
                                R.representative_id
                            };

                foreach (var representative in query.ToList())
                {
                    representatives.Add(new User(representative.first_name, representative.last_name, representative.representative_id));
                }

                for (int i = 0; i < representatives.Count; i++)
                {
                    comboBoxRepresentatives.Items.Add(representatives[i]);
                }

                if (representatives.Count > 0)
                {
                    comboBoxRepresentatives.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FillSupervisorsComboBox()
        {
            try
            {
                List<User> supervisors = new List<User>();
                var query = from R in DB.supervisors
                            select new
                            {
                                R.supervisor_id,
                                R.users
                            };

                foreach (var supervisor in query.ToList())
                {
                    supervisors.Add(new User(supervisor.users.first_name, supervisor.users.last_name, supervisor.supervisor_id));
                }

                for (int i = 0; i < supervisors.Count; i++)
                {
                    comboBoxSupervisors.Items.Add(supervisors[i]);
                }

                if (supervisors.Count > 0)
                {
                    comboBoxSupervisors.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void FillTopicsComboBox()
        {
            try
            {
                var query = (from R in DB.tasks
                            select new
                            {
                                R.topic
                            }).Distinct();
                foreach (var task in query.ToList())
                {
                    comboBoxTopics.Items.Add(task.topic);
                }

                if (comboBoxTopics.Items.Count > 0)
                {
                    comboBoxTopics.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonAddRepresentatives_Click(object sender, RoutedEventArgs e)
        {
            AddRepresentative window = new AddRepresentative(DB);
            window.Closed += delegate { FillRepresentativesComboBox(); FillGridWithRepresentatives(); };
            window.Show();
        }

        private void ButtonAddShops_Click(object sender, RoutedEventArgs e)
        {
            AddShop window = new AddShop(DB);
            window.Closed += delegate { FillGridWithShops(); };
            window.Show();
        }

        private void ButtonRepresentativesOfSupervisor_Click(object sender, RoutedEventArgs e)
        {
            if (comboBoxSupervisors.Text != "")
            {
                try
                {
                    int supervisorID = (comboBoxSupervisors.SelectedItem as User).id;
                    var query = from R in DB.representatives
                                where R.supervisors.supervisor_id == supervisorID
                                select new
                                {
                                    R.users.user_id,
                                    R.users.first_name,
                                    R.users.last_name,
                                    R.users.city,
                                    R.users.phone_number
                                };

                    dataGridTables.ItemsSource = query.ToList();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(Constants.fieldfillingWarning);
            }
        }

        private void ButtonAssignedShopsWithTopicForRepresentative_Click(object sender, RoutedEventArgs e)
        {
            if (comboBoxRepresentatives.Text != "" && comboBoxTopics.Text != "")
            {
                try
                {
                    int representativeID = (comboBoxRepresentatives.SelectedItem as User).id;
                    string taskTopic = comboBoxTopics.SelectedValue.ToString();
                    var query = from T in DB.tasks
                                where T.representatives.representative_id == representativeID
                                && T.topic == taskTopic
                                select new
                                {
                                    T.shops.shop_id,
                                    T.shops.address,
                                    T.shops.name,
                                    T.shops.representative_id
                                };

                    dataGridTables.ItemsSource = query.ToList();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show(Constants.fieldfillingWarning);
            }
        }

        private void ButtonRepresentativesWithNoShops_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var query = from R in DB.representatives
                            join S in DB.shops on R equals S.representatives into SR
                            from subR in SR.DefaultIfEmpty()
                            where subR.representative_id == null
                            select new
                            {
                                representative_first_name = R.users.first_name,
                                representative_last_name = R.users.last_name,
                                supervisors_first_name = R.supervisors.users.first_name,
                                supervisors_last_name = R.supervisors.users.last_name,
                            };

                dataGridTables.ItemsSource = query.ToList();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
