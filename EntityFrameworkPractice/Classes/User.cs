﻿namespace EntityFrameworkPractice
{
    public class User
    {
        public readonly string Text;
        public readonly int id;

        public User(string firstName, string lastName, int id)
        {
            this.id = id;
            Text = firstName + " " + lastName;
        }

        public override string ToString()
        {
            return Text;
        }
    }
}
